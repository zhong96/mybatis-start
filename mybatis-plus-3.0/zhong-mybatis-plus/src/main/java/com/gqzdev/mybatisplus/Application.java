package com.gqzdev.mybatisplus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName Application
 * @Description
 * @Author ganquanzhong
 * @Date2020/8/16 23:12
 * @Version
 **/

@SpringBootApplication

public class Application {
}
