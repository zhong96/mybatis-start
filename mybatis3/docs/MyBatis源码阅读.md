构建MyBatis源码

![](images/gqzdev-2020-07-16_17-38-38.png)

- [P1mybatis源码精讲一之JDK常用核心原理复习](https://www.bilibili.com/video/BV1BC4y1H7z5?p=1)
- [P2mybatis源码精讲二之反射机制，sql解析替换与JDK proxy原理](https://www.bilibili.com/video/BV1BC4y1H7z5?p=2)
- [P3mybatis源码精讲三之Mybatis 接口设计与Configuration的初始化](https://www.bilibili.com/video/BV1BC4y1H7z5?p=3)
- [P4mybatis源码精讲四之Mybatis Mapper生成与并发优化](https://www.bilibili.com/video/BV1BC4y1H7z5?p=4)
- [P5mybatis源码精讲五之Mybatis MapperStatement生成原理](https://www.bilibili.com/video/BV1BC4y1H7z5?p=5)
- [P6mybatis源码精讲六之Mybatis Mapper实例生成原理](https://www.bilibili.com/video/BV1BC4y1H7z5?p=6)
- [P7mybatis源码精讲七之Mybatis MappedStatement核心原理](https://www.bilibili.com/video/BV1BC4y1H7z5?p=7)
- [P8mybatis源码精讲八之Mybatis 拦截器与StatementHandler 核心原理](https://www.bilibili.com/video/BV1BC4y1H7z5?p=8)
- [P9mybatis源码精讲九之Mybatis 拦截器与MetaObject核心原理](https://www.bilibili.com/video/BV1BC4y1H7z5?p=9)
- [P10mybatis源码精讲十之Mybatis 拦截器与插件原理](https://www.bilibili.com/video/BV1BC4y1H7z5?p=10)
- [P11mybatis源码精讲十一之Mybatis MappedStatement生成原理与Spring核心接口](https://www.bilibili.com/video/BV1BC4y1H7z5?p=11)
- [P12mybatis源码精讲十二之Spring核心接口原理](https://www.bilibili.com/video/BV1BC4y1H7z5?p=12)
- [P13mybatis源码精讲十三之Spring核心接口与Mybatis-Spring原理](https://www.bilibili.com/video/BV1BC4y1H7z5?p=13)
- [P14mybatis源码精讲十四之Mybatis-Spring详解](https://www.bilibili.com/video/BV1BC4y1H7z5?p=14)
- [P15mybatis源码精讲十五之整体回顾](https://www.bilibili.com/video/BV1BC4y1H7z5?p=15)