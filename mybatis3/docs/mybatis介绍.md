MyBatis是什么？

ORM （Object Relation Mapping） 就是把字段映射为对象的属性

User表

```sql
create table user(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8mb4 ,
  `password` varchar(255) CHARACTER SET utf8mb4 ,
  `deptment` varchar(255) CHARACTER SET utf8mb4 ,
  `phone` varchar(255) CHARACTER SET utf8mb4 ,
  `email` varchar(255) CHARACTER SET utf8mb4 ,
  `status` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4,
  PRIMARY KEY (`id`) USING BTREE
)
```

在Java中映射为一个对象

```java
public class User implements Serializable {
    private static final long serialVersionUID = 783038016641480725L;

    private Integer id;

    private String userName;

    private String password;

    private String deptment;

    private String phone;

    private String email;

    private Integer status;

    private Date createDate;

    private String remark;
```

`JDBC`操作步骤

加载驱动 在8.0中已经使用com.mysql.cj.jdbc.Driver

加载类“com.mysql.jdbc.Driver”。这是弃用。新的驱动程序类是' com.mysql.cj.jdbc.Driver'。驱动程序是通过
SPI自动注册的，手动加载驱动程序类通常是不必要的。

1. 导入JDBC驱动包
2. 通过DriverManager注册驱动
3. 创建连接
4. 创建Statement
5. CRUD操作
6. 操作结果集
7. 关闭连接，释放资源



使用MyBatis操作

1. 导入JDBC驱动包，现在都是8.0版本  com.mysql.cj.jdbc.Driver
2. 引入MyBatis依赖包







使用MyBatis-Plus



