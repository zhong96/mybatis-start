/**
 *    Copyright 2009-2020 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.jdbc;

/**
 * SQL语句构建器
 *  https://mybatis.org/mybatis-3/zh/statement-builders.html
 * MyBatis 3 提供了方便的工具类来帮助解决此问题。借助 SQL 类，我们只需要简单地创建一个实例，并调用它的方法即可生成 SQL 语句。
 *
 * @author Clinton Begin
 */
public class SQL extends AbstractSQL<SQL> {

  @Override
  public SQL getSelf() {
    return this;
  }

}
/**
 *  使用String类型进行sql拼接，很容易出错
 * String sql = "SELECT P.ID, P.USERNAME, P.PASSWORD, P.FULL_NAME, "
 * "P.LAST_NAME,P.CREATED_ON, P.UPDATED_ON " +
 * "FROM PERSON P, ACCOUNT A " +
 * "INNER JOIN DEPARTMENT D on D.ID = P.DEPARTMENT_ID " +
 * "INNER JOIN COMPANY C on D.COMPANY_ID = C.ID " +
 * "WHERE (P.ID = A.ID AND P.FIRST_NAME like ?) " +
 * "OR (P.LAST_NAME like ?) " +
 * "GROUP BY P.ID " +
 * "HAVING (P.LAST_NAME like ?) " +
 * "OR (P.FIRST_NAME like ?) " +
 * "ORDER BY P.ID, P.FULL_NAME";
 */

/**
 *  使用SQL类构建
 * private String selectPersonSql() {
 *   return new SQL() {{
 *     SELECT("P.ID, P.USERNAME, P.PASSWORD, P.FULL_NAME");
 *     SELECT("P.LAST_NAME, P.CREATED_ON, P.UPDATED_ON");
 *     FROM("PERSON P");
 *     FROM("ACCOUNT A");
 *     INNER_JOIN("DEPARTMENT D on D.ID = P.DEPARTMENT_ID");
 *     INNER_JOIN("COMPANY C on D.COMPANY_ID = C.ID");
 *     WHERE("P.ID = A.ID");
 *     WHERE("P.FIRST_NAME like ?");
 *     OR();
 *     WHERE("P.LAST_NAME like ?");
 *     GROUP_BY("P.ID");
 *     HAVING("P.LAST_NAME like ?");
 *     OR();
 *     HAVING("P.FIRST_NAME like ?");
 *     ORDER_BY("P.ID");
 *     ORDER_BY("P.FULL_NAME");
 *   }}.toString();
 * }
 */


