/**
 *    Copyright 2009-2020 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.session;

import java.sql.Connection;

/**
 * 事务隔离级别 枚举类型
 * 事务隔离级别支持 JDBC 的五个隔离级别
 * none   不支持事务
 * read_commited    读已提交
 * read_uncommited  读未提交
 * repeatable_read  可重复读
 * serializable   序列化
 *
 * @author Clinton Begin
 */
public enum TransactionIsolationLevel {
  /**
   *  int TRANSACTION_NONE             = 0;
   */
  NONE(Connection.TRANSACTION_NONE),
  /**
   *  1
   *  指示可能发生脏读、不可重复读和幻像读的常数。
   *  这个级别允许由一个事务更改的行在提交对该行的任何更改之前被另一个事务读取(“脏读”)。
   *  如果任何更改被回滚，第二个事务将检索到无效的行。
   */
  READ_UNCOMMITTED(Connection.TRANSACTION_READ_UNCOMMITTED),
  /**
   *  2
   *  指示防止脏读的常量;可能会发生不可重复读取和幻像读取。
   *  此级别仅禁止事务读取包含未提交更改的行。
   */
  READ_COMMITTED(Connection.TRANSACTION_READ_COMMITTED),
  /**
   *  4
   *  指示防止脏读和不可重复读;可能会发生幻像读取。
   *  这个级别禁止事务读取包含未提交更改的行，
   *  也禁止以下情况:一个事务读取一行，第二个事务修改行，
   *  第一个事务重新读取行，第二次获得不同的值(“不可重复读取”)。
   */
  REPEATABLE_READ(Connection.TRANSACTION_REPEATABLE_READ),
  /**
   *  8
   *  一个常数，表示防止脏读、不可重复读和幻影读。
   *  这个水平进一步包括TRANSACTION_REPEATABLE_READ时的禁令,
   *  禁止在一个事务中读取所有行,满足条件,第二个事务插入一行,
   *  满足条件,和第一个事务重读同等条件下,获取额外的“幽灵”在第二行阅读。
   */
  SERIALIZABLE(Connection.TRANSACTION_SERIALIZABLE);

  private final int level;

  TransactionIsolationLevel(int level) {
    this.level = level;
  }

  public int getLevel() {
    return level;
  }
}
