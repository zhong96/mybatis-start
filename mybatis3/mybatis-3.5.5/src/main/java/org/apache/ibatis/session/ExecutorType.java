/**
 *    Copyright 2009-2020 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.session;

/**
 * @author Clinton Begin
 */
/*
    mybatis的执行器类型SIMPLE, REUSE, BATCH
    简单  复用  批量
 */
/*
 * ExecutorType.SIMPLE：
 *      该类型的执行器没有特别的行为。它为每个语句的执行创建一个新的预处理语句。
 * ExecutorType.REUSE：
 *      该类型的执行器会复用预处理语句。
 * ExecutorType.BATCH：
 *      该类型的执行器会批量执行所有更新语句，如果 SELECT 在多个更新中间执行，
 *      将在必要时将多条更新语句分隔开来，以方便理解。
 * @author: ganquanzhong
 */
public enum ExecutorType {
  SIMPLE, REUSE, BATCH
}
